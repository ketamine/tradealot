name := "TradeALot"

version := "1.0"

scalaVersion := "2.10.3"

resolvers += "Typesafe repository" at "http://repo.typesafe.com/typesafe/releases/"

libraryDependencies += "io.netty" % "netty-all" % "4.0.21.Final"

libraryDependencies += "org.springframework" % "spring-context" % "4.0.6.RELEASE"

libraryDependencies += "org.springframework" % "spring-jdbc" % "4.0.6.RELEASE"

libraryDependencies += "org.springframework" % "spring-tx" % "4.0.6.RELEASE"

libraryDependencies += "com.jolbox" % "bonecp" % "0.8.0.RELEASE"

libraryDependencies += "mysql" % "mysql-connector-java" % "5.1.34"

libraryDependencies += "com.fasterxml.jackson.core" % "jackson-databind" % "2.4.1.3"

libraryDependencies += "com.fasterxml.jackson.module" % "jackson-module-scala_2.10" % "2.4.1"

libraryDependencies += "org.scalatest" % "scalatest_2.10" % "2.2.1" % "test"

libraryDependencies += "com.typesafe.akka" % "akka-actor_2.10" % "2.3.5"



