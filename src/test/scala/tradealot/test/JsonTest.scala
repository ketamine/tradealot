package tradealot.test

import com.fasterxml.jackson.databind.{DeserializationFeature, ObjectMapper}
import com.fasterxml.jackson.module.scala.DefaultScalaModule
import org.scalatest.{FlatSpec, Matchers}
import tradealot.messages.{InboundMessage, OutgoingMessage, Command}

class JsonTest extends FlatSpec with Matchers {

  it should "Serialize case object into json" in {
    val mapper: ObjectMapper = new ObjectMapper()
    mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
    mapper.registerModule(DefaultScalaModule)

    val echo: Any = Command("WTF")
    val serialized = mapper.writeValueAsString(echo)

    val parsed = mapper.readValue(serialized, classOf[Command])


    parsed shouldEqual echo
  }
}
