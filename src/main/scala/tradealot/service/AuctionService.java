package tradealot.service;

import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.transaction.annotation.Transactional;
import tradealot.messages.AuctionRequest;
import tradealot.messages.Bid;

@Transactional
public class AuctionService {


    private JdbcTemplate jdbcTemplate;

    public AuctionService(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    public Integer createAuction(AuctionRequest auctionRequest, String userId) {
        jdbcTemplate
                .update("insert into Auction (productId, startingPrice, owner, phoneCondition) values (?, ?, ?, ?)",
                        auctionRequest.productId(), auctionRequest.startingPrice().bigDecimal(), userId, auctionRequest.condition());
        return jdbcTemplate.queryForObject("SELECT LAST_INSERT_ID()", Integer.class);
    }

    public void placeBid(Bid bid) {
        jdbcTemplate.update("insert into Bid (bidder, amount, auctionId) values (?, ?, ?)",
                bid.bidder(), bid.amount().bigDecimal(), bid.auctionId());
    }

    public void closeAuction(Integer auctionId, String winner) {
        jdbcTemplate.update("update Auction set winner = ?, active = FALSE where auctionId = ?", winner, auctionId);
    }
}
