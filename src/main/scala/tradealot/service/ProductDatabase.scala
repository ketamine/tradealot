package tradealot.service

object ProductDatabase {

  private val prodMap = Map(
    "Samsung" -> Map(
      "Note 3 32GB" -> 698,
      "S5 16GB" -> 609
    ),
    "Sony" -> Map(
      "Xperia Z2 16GB" -> 599,
      "Xperia M2 16GB" -> 289
    ),
    "LG" -> Map(
      "Google Nexus 5 32GB" -> 424,
      "G3 32GB" -> 619,
      "G2 32GB" -> 389
    ),
    "HTC" -> Map(
      "One M8 16GB" -> 599
    )
  )

  def getManufacturers: List[String] = prodMap.keys.toList

  def getProducts(manufacturer: String): Option[List[String]] = {
    prodMap.get(manufacturer).flatMap {
      case prod => Some(prod.keys.toList)
    }
  }

}
