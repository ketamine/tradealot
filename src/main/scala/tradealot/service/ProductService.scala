package tradealot.service

import java.sql.ResultSet
import javax.annotation.PostConstruct

import org.springframework.jdbc.core.{JdbcTemplate, RowCallbackHandler, RowMapper}
import org.springframework.transaction.annotation.Transactional
import tradealot.messages.ProductInfo

import scala.collection.mutable

@Transactional
class ProductService(jdbcTemplate: JdbcTemplate) {

  import scala.collection.JavaConverters._

  private val productCache = new mutable.HashMap[Int, ProductInfo]()

  val rowMapper = new RowMapper[String] {
    override def mapRow(rs: ResultSet, rowNum: Int): String = rs.getString("manufacturer")
  }

  @PostConstruct
  def init () {
    jdbcTemplate.query("select id, manufacturer, model from Phone", new RowCallbackHandler {
      override def processRow(rs: ResultSet) {
        val manufacturer = rs.getString("manufacturer")
        val model = rs.getString("model")
        val id = rs.getInt("id")
        productCache += id -> ProductInfo(id, model, manufacturer)
      }
    })
  }

  def productInfo (id: Int): ProductInfo = {
    productCache.get(id).get
  }

  def getManufacturers: List[String] = jdbcTemplate.query("SELECT manufacturer FROM Phone GROUP BY manufacturer", rowMapper).asScala.toList

  def getProducts(manufacturer: String): Map[Int, String] = {
    val result = new mutable.HashMap[Int, String]()
    jdbcTemplate.query("SELECT id, model FROM Phone where manufacturer = ?", new RowCallbackHandler {
      override def processRow(rs: ResultSet): Unit = result += rs.getInt("id") -> rs.getString("model")
    }, manufacturer)

    result.toMap
  }


}
