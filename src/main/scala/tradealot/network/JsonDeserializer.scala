package tradealot.network

import akka.actor.ActorSystem
import com.fasterxml.jackson.databind.{DeserializationFeature, JsonNode, ObjectMapper}
import com.fasterxml.jackson.module.scala.DefaultScalaModule
import io.netty.channel.ChannelHandler.Sharable
import io.netty.channel.{ChannelHandlerContext, SimpleChannelInboundHandler}
import io.netty.handler.codec.http.websocketx.TextWebSocketFrame
import tradealot.ActorPaths
import tradealot.messages.{InboundMessageRouting, InboundMessage, OutgoingMessage}

@Sharable
class JsonDeserializer(messageClasses: Map[String, Class[_ <: AnyRef]],
                       actorSystem: ActorSystem)
  extends SimpleChannelInboundHandler[TextWebSocketFrame] with ActorPaths {

  private val objectMapper: ObjectMapper = new ObjectMapper()
  objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false)
  objectMapper.registerModule(DefaultScalaModule)

  override def channelRead0(ctx: ChannelHandlerContext, msg: TextWebSocketFrame) = {
    val tree: JsonNode = objectMapper.readTree(msg.text())
    val messageType = messageClasses.get(tree.get("messageType").asText())
    messageType.collect {
      case cls: Class[_] =>
        val userId = ChannelAttributeUtil.getUserId(ctx.channel())
        val gatewayRef = ChannelAttributeUtil.getGateway(ctx.channel())
        val messageId = tree.get("messageId").asInt()
        try {
          val message = objectMapper.treeToValue(tree, messageType.get)
          gatewayRef ! InboundMessage(InboundMessageRouting(userId, messageId), message)
        } catch {
          case e: Exception =>
            e.printStackTrace()
            gatewayRef ! OutgoingMessage(messageId, "Malformed message", failure = true)
        }
    }

  }
}
