package tradealot.network

import akka.actor.{ActorSystem, Props}
import io.netty.channel.ChannelHandler.Sharable
import io.netty.channel._
import io.netty.handler.codec.http.HttpHeaders.Names
import io.netty.handler.codec.http._
import io.netty.handler.codec.http.websocketx.WebSocketServerHandshakerFactory
import tradealot.ActorPaths
import tradealot.actor.MessageGateway

@Sharable
class ConnectionInitiator(actorSystem: ActorSystem)
  extends SimpleChannelInboundHandler[FullHttpRequest] with ActorPaths {


  private val connectionPath: String = "/commence"

  override def channelRead0(ctx: ChannelHandlerContext, request: FullHttpRequest) = {
    val uri = request.getUri
    val channel = ctx.channel()
    if (uri.startsWith(connectionPath)) {

      val handShaker = new WebSocketServerHandshakerFactory(getWebSocketLocation(request), null, false).newHandshaker(request)

      if (handShaker == null) {
        WebSocketServerHandshakerFactory.sendUnsupportedVersionResponse(channel)
      } else {
        val future: ChannelFuture = handShaker.handshake(channel, request)

        future.addListener(HandshakeSuccessCallback)
      }
    } else {
      ctx.fireChannelRead(request.retain())
    }
  }

  private def getWebSocketLocation(req: FullHttpRequest): String = {
    "ws://" + req.headers().get(Names.HOST) + connectionPath
  }

  private object HandshakeSuccessCallback extends ChannelFutureListener {
    override def operationComplete(channelFuture: ChannelFuture) = {

      val channel = channelFuture.channel()
      val userId = ChannelAttributeUtil.getUserId(channel)
      val gateway = actorSystem.actorOf(Props(classOf[MessageGateway], userId, channel))
      ChannelAttributeUtil.setGateway(channel, gateway)
    }
  }

}
