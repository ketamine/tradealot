package tradealot.network

import io.netty.channel.ChannelInitializer
import io.netty.channel.socket.SocketChannel
import io.netty.handler.codec.http.{HttpObjectAggregator, HttpServerCodec}

class ServerChannelInitializer(staticFileHandler: StaticFileHandler,
                               jsonDeserializer: JsonDeserializer,
                               connectionInitiator: ConnectionInitiator)
  extends ChannelInitializer[SocketChannel] {

  val securityHandler = new SecurityHandler
  val jsonSerializer = new JsonSerializer

  def initChannel(channel: SocketChannel) {
    val pipeline = channel.pipeline()
    pipeline.addLast("codec-http", new HttpServerCodec())
    pipeline.addLast("aggregator", new HttpObjectAggregator(65536))
    pipeline.addLast("securityHandler", securityHandler)
    pipeline.addLast("connectionInitiator", connectionInitiator)
    pipeline.addLast("jsonDeserializer", jsonDeserializer)
    pipeline.addLast("jsonSerializer", jsonSerializer)
    pipeline.addLast("staticFileHandler", staticFileHandler)
  }

}