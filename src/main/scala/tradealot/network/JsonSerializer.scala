package tradealot.network

import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.module.scala.DefaultScalaModule
import io.netty.buffer.Unpooled
import io.netty.channel.ChannelHandler.Sharable
import io.netty.channel.{ChannelPromise, ChannelHandlerContext, ChannelOutboundHandlerAdapter}
import io.netty.handler.codec.http.websocketx.TextWebSocketFrame
import tradealot.messages.{ChannelWritable, Notification, OutgoingMessage}

@Sharable
class JsonSerializer extends ChannelOutboundHandlerAdapter {

  private val objectMapper = new ObjectMapper()

  objectMapper.registerModule(DefaultScalaModule)


  override def write(ctx: ChannelHandlerContext, msg: scala.Any, promise: ChannelPromise) = {
    msg match {
      case msg: ChannelWritable =>
        val bytes = objectMapper.writeValueAsBytes(msg)
        ctx.writeAndFlush(new TextWebSocketFrame(Unpooled.copiedBuffer(bytes)))
      case _ => ctx.write(msg, promise)
    }
  }

}