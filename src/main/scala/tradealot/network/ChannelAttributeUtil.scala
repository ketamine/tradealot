package tradealot.network

import akka.actor.ActorRef
import io.netty.channel.Channel
import io.netty.util.AttributeKey

object ChannelAttributeUtil {

  private val userIdAttributeKey: AttributeKey[String] = AttributeKey.valueOf("userId")
  private val gatewayAttributeKey: AttributeKey[ActorRef] = AttributeKey.valueOf("gateway")

  def getUserId(channel: Channel): String = channel.attr(userIdAttributeKey).get()
  def setUserId(channel: Channel, userId: String) = channel.attr(userIdAttributeKey).set(userId)

  def setGateway(channel: Channel, gateway: ActorRef) = channel.attr(gatewayAttributeKey).set(gateway)
  def getGateway(channel: Channel) : ActorRef = channel.attr(gatewayAttributeKey).get()
}
