package tradealot.network

import java.util.UUID

import io.netty.channel.ChannelHandler.Sharable
import io.netty.channel.{ChannelFutureListener, ChannelHandlerContext, SimpleChannelInboundHandler}
import io.netty.handler.codec.http.HttpHeaders.Names
import io.netty.handler.codec.http._
import io.netty.handler.codec.http.multipart.InterfaceHttpData.HttpDataType
import io.netty.handler.codec.http.multipart.{Attribute, DefaultHttpDataFactory, HttpPostRequestDecoder}

import scala.collection.mutable

@Sharable
class SecurityHandler extends SimpleChannelInboundHandler[FullHttpRequest] {

  import scala.collection.JavaConversions._

  private val authUri = "/authenticate"
  private val successUri = "/app.html"
  private val loginFormUri = "/login.html"
  private val sessionCookieKey = "sessionId"

  private val sessionMap = new mutable.HashMap[String, String]()


  override def channelRead0(ctx: ChannelHandlerContext, request: FullHttpRequest): Unit = {
    val uri = request.getUri

    if (uri == loginFormUri || uri.matches("/css/.*.css$")) {
      ctx.fireChannelRead(request.retain())
      return
    }

    if (uri == authUri) {

      if (request.getMethod != HttpMethod.POST) {
        val response = new DefaultFullHttpResponse(HttpVersion.HTTP_1_1, HttpResponseStatus.BAD_REQUEST)
        respond(ctx, response)
        return
      }

      processLogin(ctx, request)
      return
    }

    val cookie = request.headers().get(Names.COOKIE)

    if (cookie == null) {
      onFailure(ctx);
      return
    }

    val sessionId = CookieDecoder.decode(cookie).map(a => a.getName -> a.getValue).toMap.get(sessionCookieKey)
    sessionId.flatMap(sessionMap.get) match {
      case Some(uid) => {
        ChannelAttributeUtil.setUserId(ctx.channel(), uid)
        ctx.fireChannelRead(request.retain())
      }
      case _ => onFailure(ctx)
    }


  }

  def checkCredentials(login: String, password: String) = password == "1"

  def onFailure(context: ChannelHandlerContext) = {
    val response = new DefaultFullHttpResponse(HttpVersion.HTTP_1_1, HttpResponseStatus.FOUND)
    response.headers().add(Names.LOCATION, loginFormUri)
    respond(context, response)
  }

  def processLogin(context: ChannelHandlerContext, request: FullHttpRequest) = {
    implicit val decoder = new HttpPostRequestDecoder(new DefaultHttpDataFactory(false), request)

    (attr("userId"), attr("password")) match {
      case (Some(login), Some(password)) => {
        if (checkCredentials(login, password)) {
          onSuccess(context, login)
        } else {
          onFailure(context)
        }
      }
      case _ => onFailure(context);
    }

  }

  def onSuccess(context: ChannelHandlerContext, userId: String) {
    val uuid = UUID.randomUUID().toString
    ChannelAttributeUtil.setUserId(context.channel(), userId)
    sessionMap += uuid -> userId

    val response = new DefaultFullHttpResponse(HttpVersion.HTTP_1_1, HttpResponseStatus.FOUND)
    response.headers().add(Names.LOCATION, successUri)
    response.headers().add(Names.SET_COOKIE, ServerCookieEncoder.encode(sessionCookieKey, uuid))
    respond(context, response)
  }

  def attr(key: String)(implicit decoder: HttpPostRequestDecoder): Option[String] = {
    val data = decoder.getBodyHttpData(key)

    if (data == null) {
      return None
    }

    if (data.getHttpDataType == HttpDataType.Attribute) {
      val value = data.asInstanceOf[Attribute].getValue
      Some(value)
    } else {
      None
    }
  }

  def respond(context: ChannelHandlerContext, response: FullHttpResponse) {
    HttpHeaders.setContentLength(response, response.content().readableBytes())
    response.headers().add(Names.CONTENT_TYPE, "application/octet-stream")

    context.channel().writeAndFlush(response).addListener(ChannelFutureListener.CLOSE)
  }

}