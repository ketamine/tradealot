package tradealot.network

import java.io.{File, FileNotFoundException, RandomAccessFile}
import java.net.URLDecoder
import java.text.SimpleDateFormat
import java.util.regex.Pattern
import java.util.{Calendar, Date, GregorianCalendar, Locale}
import javax.activation.MimetypesFileTypeMap

import io.netty.buffer.Unpooled
import io.netty.channel.ChannelHandler.Sharable
import io.netty.channel._
import io.netty.handler.codec.http.HttpHeaders.Names
import io.netty.handler.codec.http._
import io.netty.util.CharsetUtil

@Sharable
class StaticFileHandler(resourceDirectory: String)
  extends SimpleChannelInboundHandler[HttpRequest] {

  val ALLOWED_FILE_NAME = Pattern.compile("[A-Za-z0-9][-_A-Za-z0-9\\.]*")
  val INSECURE_URI = Pattern.compile(".*[<>&\"].*")
  val dateFormatter = new SimpleDateFormat("EEE, dd MMM yyyy HH:mm:ss zzz", Locale.US)
  val HTTP_CACHE_SECONDS = 60
  val mimeTypesMap = new MimetypesFileTypeMap()

  mimeTypesMap.addMimeTypes("text/css   css")
  mimeTypesMap.addMimeTypes("text/javascript  js")

  override def channelRead0(context: ChannelHandlerContext, request: HttpRequest): Unit = {
    val uri: String = request.getUri
    val path = sanitizeUri(uri)
    if (path == null) {
      sendError(context, HttpResponseStatus.SERVICE_UNAVAILABLE)
      return
    }

    val file = new File(path)
    if (file.isHidden || !file.exists() || file.isDirectory) {
      sendError(context, HttpResponseStatus.NOT_FOUND)
      return
    }

    if (!file.isFile) {
      sendError(context, HttpResponseStatus.FORBIDDEN)
      return
    }

    val fileOption: Option[RandomAccessFile] = try {
      Some(new RandomAccessFile(file, "r"))
    } catch {
      case nf: FileNotFoundException => None
    }

    if (!fileOption.isDefined) {
      sendError(context, HttpResponseStatus.NOT_FOUND)
      return
    }

    val randomAccessFile = fileOption.get


    val fileLength = randomAccessFile.length()

    val response = new DefaultHttpResponse(HttpVersion.HTTP_1_1, HttpResponseStatus.OK)
    HttpHeaders.setContentLength(response, fileLength)
    setContentTypeHeader(response, file)
    setDateAndCacheHeaders(response, file)

    context.write(response)
    context.write(new DefaultFileRegion(randomAccessFile.getChannel, 0, fileLength))

    val lastContentFuture: ChannelFuture = context.writeAndFlush(LastHttpContent.EMPTY_LAST_CONTENT)
    lastContentFuture.addListener(ChannelFutureListener.CLOSE)
  }

  def sendError(ctx: ChannelHandlerContext, status: HttpResponseStatus) = {
    val response = new DefaultFullHttpResponse(
      HttpVersion.HTTP_1_1, status, Unpooled.copiedBuffer("Failure: " + status.toString + "\r\n", CharsetUtil.UTF_8))
    response.headers().set(Names.CONTENT_TYPE, "text/plain; charset=UTF-8")

    ctx.writeAndFlush(response).addListener(ChannelFutureListener.CLOSE)
  }

  def sendNotModified(ctx: ChannelHandlerContext) = {
    val response = new DefaultFullHttpResponse(HttpVersion.HTTP_1_1, HttpResponseStatus.NOT_MODIFIED)
    setDateHeader(response)

    ctx.writeAndFlush(response).addListener(ChannelFutureListener.CLOSE)
  }

  private def sanitizeUri(input: String): String = {
    var uri: String = URLDecoder.decode(input, "UTF-8")

    if (!uri.startsWith("/")) {
      return null
    }

    uri = uri.replace('/', File.separatorChar)

    if (uri.contains(File.separator + '.') ||
      uri.contains('.' + File.separator) ||
      uri.startsWith(".") || uri.endsWith(".") ||
      INSECURE_URI.matcher(uri).matches()) {
      return null
    }

    resourceDirectory + File.separator + uri
  }

  def setDateHeader(response: FullHttpResponse) = {
    response.headers().set(Names.DATE, dateFormatter.format(new Date()))
  }


  def setDateAndCacheHeaders(response: HttpResponse, fileToCache: File) = {

    val calendar = new GregorianCalendar()
    response.headers().set(Names.DATE, dateFormatter.format(calendar.getTime))

    calendar.add(Calendar.SECOND, HTTP_CACHE_SECONDS)

    response.headers().set(Names.EXPIRES, dateFormatter.format(calendar.getTime))
    response.headers().set(Names.CACHE_CONTROL, "private, max-age=" + HTTP_CACHE_SECONDS)
    response.headers().set(Names.LAST_MODIFIED, dateFormatter.format(new Date(fileToCache.lastModified())))
  }


  def setContentTypeHeader(response: HttpResponse, file: File) = {
    response.headers().set(Names.CONTENT_TYPE, mimeTypesMap.getContentType(file.getPath))
  }
}
