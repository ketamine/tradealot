package tradealot.actor

import java.util.Date

import akka.actor.{ActorRef, ActorSelection, Cancellable}
import tradealot.messages._
import tradealot.service.AuctionService

import scala.collection.mutable
import scala.concurrent.Future
import scala.util.{Failure, Success}

class Auction(auctionService: AuctionService) extends MessagingActor {

  import context.dispatcher
  import scala.concurrent.duration._

  val timeoutInitial = 120
  val timeoutDecrement = 10

  var timeout: Option[Int] = None
  var timerCancellable: Option[Cancellable] = None
  var lastBidReceivedAt: Option[Date] = None

  var open = true

  var currentPrice: BigDecimal = 0
  var lot: Lot = _

  val bids = new mutable.Stack[Bid]
  val bidBacklog = new mutable.Queue[(Bid, InboundMessageRouting)]()
  var bidSaveInProgress = false

  val participants = new mutable.HashSet[ActorRef]()

  def selfId = lot.auctionId

  override def preStart() {
    log.info("Starting auction, lot " + lot.productInfo.manufacturer + " " + lot.productInfo.model + " from user " + lot.ownerId)
  }

  var ownerRef: ActorSelection = _

  def init(auctionInfo: AuctionInfo) {
    lot = auctionInfo.lot
    currentPrice = auctionInfo.price
    ownerRef = user(lot.ownerId)
  }

  override def onInboundMessage = {
    case _ if !open => reply("Auction is closing", failure = true)
    case joinRequest: JoinAuction => processJoinRequest(joinRequest)
    case gas: GetAuctionStatus => status()
    case bidRequest: BidRequest => processBidRequest(bidRequest)
  }

  override def onGenericMessage = {
    case DequeueBid => drainBacklog()
    case TimeUp => initiateFinish()
  }

  def status() {
    val status = AuctionStatus(AuctionInfo(lot, currentPrice, Some(RemainingTime(selfId, lastBidReceivedAt, timeout))), bids.toList)
    reply(status)
  }


  def closeAuction() {
    val auctionFinished = AuctionFinished(selfId, bids.top.bidder)
    ownerRef ! auctionFinished

    notificationDispatcher() ! auctionFinished

    participants foreach {
      _ ! auctionFinished
    }

    context.parent ! TerminateAuction(selfId)
    context.stop(self)
  }


  def initiateFinish() {
    open = false
    log.debug("Closing auction " + selfId)
    Future(auctionService.closeAuction(selfId, bids.top.bidder)) onComplete {
      case Success(_) =>
        closeAuction()
      case Failure(e) =>
        log.error(e, "Could not save auction")
    }
  }

  def addBid(bid: Bid, routing: InboundMessageRouting) {
    if (bid.amount <= currentPrice) {
      routing.reply(BidRejected("Your bid is lower than current top bid of " + currentPrice), failure = true)
      self ! DequeueBid
      return
    }

    persistBid(bid, routing)
  }

  def processBidRequest(request: BidRequest) {
    val bid = Bid(selfId, senderName, request.amount)

    if (bidSaveInProgress) {
      bidBacklog += bid -> routing
    } else {
      addBid(bid, routing)
    }

  }

  def persistBid(bid: Bid, routing: InboundMessageRouting) {
    bidSaveInProgress = true
    val future = Future(auctionService.placeBid(bid))

    future.andThen {
      case _ =>
        self ! DequeueBid
        bidSaveInProgress = false
    } onComplete {
      case Success(_) =>
        finalizeBid(bid, routing)
      case Failure(e) =>
        log.error(e, "Could not persist bid")
        routing.reply(BidRejected("Internal error occurred. Sorry :("))
    }
  }

  def drainBacklog() {
    if (bidBacklog.nonEmpty) {
      val workItem = bidBacklog.dequeue()
      addBid(workItem._1, workItem._2)
    }
  }

  def finalizeBid(bid: Bid, routing: InboundMessageRouting) {
    currentPrice = bid.amount
    bids.push(bid)

    routing.reply(BidAccepted())

    lastBidReceivedAt = Some(new Date())

    timeout match {
      case Some(t) =>
        timeout = Some(math.max(t - timeoutDecrement, timeoutDecrement))
      case None =>
        timeout = Some(timeoutInitial)
    }

    sendNotifications(bid)
    armTimer()
  }

  def armTimer() {
    timerCancellable.collect {
      case c => c.cancel()
    }

    val cancellable = context.system.scheduler.scheduleOnce(timeout.get.seconds, self, TimeUp)
    timerCancellable = Some(cancellable)
  }

  def sendNotifications(bid: Bid) {
    val remainingTime = RemainingTime(selfId, lastBidReceivedAt, timeout)

    participants foreach { p =>
      p ! bid
      p ! remainingTime
    }

    ownerRef ! bid
    ownerRef ! remainingTime

    val priceUpdate = PriceUpdate(selfId, currentPrice)
    notificationDispatcher() ! priceUpdate
    notificationDispatcher() ! remainingTime
    context.parent ! priceUpdate
    context.parent ! remainingTime
  }

  def processJoinRequest(joinRequest: JoinAuction) {
    val requester = sender()

    val alreadyJoined = participants.contains(requester)
    val isOwner = lot.ownerId == requester.path.name

    if (alreadyJoined || isOwner) {
      reply("You cannot join this auction", failure = true)
    } else {
      reply(AuctionJoined(selfId))
      participants += requester
    }
  }

  private object DequeueBid

  private object TimeUp

}
