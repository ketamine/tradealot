package tradealot.actor

import akka.actor.{Terminated, ActorRef}
import tradealot.messages._
import tradealot.service.{ProductService, AuctionService}
import tradealot.spring.AppContext

import scala.collection.mutable
import scala.concurrent.Future
import scala.util.{Failure, Success}

class AuctionManager(auctionsService: AuctionService, productService: ProductService) extends MessagingActor {

  import context.dispatcher

  private val activeAuctions = new mutable.HashMap[Int, AuctionInfo]()
  private val auctionRefs = new mutable.HashMap[Int, ActorRef]()
  private var snapshot: List[AuctionInfo] = Nil

  override def onInboundMessage = {
    case auctionRequest: AuctionRequest => createAuction(auctionRequest)
    case Command(Command.GetActiveAuctions) => reply(snapshot)
  }

  def updateRemainingTime(remainingTime: RemainingTime) {
    val id = remainingTime.auctionId
    activeAuctions.get(id).collect {
      case a =>
        activeAuctions += id -> a.copy(remainingTime = Some(remainingTime))
    }
    updateSnapshot()
  }

  override def onGenericMessage = {
    case priceUpdate: PriceUpdate => updatePrice(priceUpdate)
    case TerminateAuction(id) =>
      auctionRefs -= id
      activeAuctions -= id
      updateSnapshot()
    case remainingTime: RemainingTime => updateRemainingTime(remainingTime)
  }

  def createAuction(auctionRequest: AuctionRequest) {
    val route = routing
    val userId = senderName
    val condition = auctionRequest.condition
    val price = auctionRequest.startingPrice

    Future {
      auctionsService.createAuction(auctionRequest, userId)
    } onComplete {
      case Success(auctionId) =>
        val productInfo = productService.productInfo(auctionRequest.productId)
        val lot = Lot(auctionId, productInfo, condition, userId)
        val auctionInfo = AuctionInfo(lot, price, None)

        val auctionRef = deployActor(auctionInfo)
        activeAuctions.update(auctionId, auctionInfo)
        auctionRefs.update(auctionId, auctionRef)
        notificationDispatcher() ! auctionInfo

        route.reply(AuctionCreated(auctionId))

        updateSnapshot()
      case Failure(ex) =>
        log.error(ex, "Unable to create auction")
        route.reply(InternalErrorAlert(), failure = true)
    }
  }

  def updateSnapshot() {
    snapshot = activeAuctions.map { case (k, info) => info}.toList
  }

  def deployActor(auctionInfo: AuctionInfo): ActorRef = {
    val initializer = { (auction: Auction) =>
      auction.init(auctionInfo);
    }

    val props = AppContext.props(classOf[Auction], initializer)
    context.actorOf(props, auctionInfo.lot.auctionId.toString)
  }

  def updatePrice(update: PriceUpdate) {
    val auctionId = update.auctionId
    activeAuctions.get(auctionId).collect { case info =>
      activeAuctions += auctionId -> info.copy(price = update.price)
    }
    updateSnapshot()
  }

}


