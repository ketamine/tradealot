package tradealot.actor

import akka.actor.{Actor, ActorRef}
import io.netty.channel.{Channel, ChannelFuture, ChannelFutureListener}
import io.netty.handler.codec.http.websocketx.CloseWebSocketFrame
import tradealot.ActorPaths
import tradealot.messages._

class MessageGateway(userId: String, channel: Channel) extends Actor with ActorPaths {

  private var user: Option[ActorRef] = None

  override def preStart() = {
    channel.closeFuture().addListener(new ChannelFutureListener {
      override def operationComplete(future: ChannelFuture) = context.stop(self)
    })

    path("users") ! ChannelLinkageRequest(userId, self)
  }

  def receive = {
    case CloseGateway(reason) =>
      val frame = new CloseWebSocketFrame(999, reason)
      channel.writeAndFlush(frame).addListener(ChannelFutureListener.CLOSE)
    case ChannelLinked() => user = Some(sender())
    case msg: InboundMessage => user.collect { case u => u ! msg}
    case msg: ChannelWritable => channel.writeAndFlush(msg)
  }
}
