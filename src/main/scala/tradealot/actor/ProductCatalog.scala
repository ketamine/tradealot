package tradealot.actor

import tradealot.messages.{Command, GetProducts, InboundMessage}
import tradealot.service.ProductService

import scala.concurrent.Future

class ProductCatalog(productService: ProductService) extends MessagingActor {

  import context.dispatcher

  override def onInboundMessage = {
    case Command(Command.GetManufacturers) =>
      async {
        productService.getManufacturers
      }
    case GetProducts(manufacturer) =>
      async {
        productService.getProducts(manufacturer)
      }
  }

  private def async[T](computation: => T) {
    val currentMessage = inboundMessage

    val future: Future[T] = Future(computation)
    future onSuccess {
      case result => currentMessage.reply(result)
    }

    future onFailure {
      case e: Exception =>
        log.error(e, "Uh-oh")
        currentMessage.reply("Failed", failure = true)
    }
  }
}
