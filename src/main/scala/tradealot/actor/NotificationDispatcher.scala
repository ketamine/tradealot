package tradealot.actor

import akka.actor.{ActorRef, Terminated}
import tradealot.messages._

import scala.collection.mutable
import scala.collection.mutable.ArrayBuffer

class NotificationDispatcher extends MessagingActor {

  import scala.concurrent.duration._
  import context._

  private val subscribers: mutable.Buffer[ActorRef] = new ArrayBuffer[ActorRef]()
  private val priceUpdates = new mutable.HashMap[Int, BigDecimal]
  private val finishedAuctions = new mutable.ArrayBuffer[Int]
  private val remainingTimes = new mutable.ArrayBuffer[RemainingTime]

  override def preStart() {
    context.system.scheduler.schedule(Duration.Zero, 2.seconds, self, FlushBuffer)
  }

  override def onGenericMessage = {
    case request: NotificationSubscriptionRequest =>
      val subscriber = sender()
      subscribers += subscriber
      context.watch(subscriber)
    case alert: AuctionInfo =>
      subscribers.foreach {
        _ ! alert
      }
    case Terminated(ref) => subscribers -= ref

    case update: PriceUpdate =>
      priceUpdates += update.auctionId -> update.price

    case AuctionFinished(id, _) =>
      finishedAuctions += id

    case r: RemainingTime =>
      remainingTimes += r
    case FlushBuffer => flushBuffer()
  }

  def flushBuffer() {
    if (priceUpdates.isEmpty && finishedAuctions.isEmpty) {
      return
    }

    val update = UpdatePackage(priceUpdates.toMap, finishedAuctions.toList, remainingTimes.toList)
    subscribers.foreach {
      _ ! update
    }

    priceUpdates.clear()
    finishedAuctions.clear()
    remainingTimes.clear()
  }

  private object FlushBuffer

}
