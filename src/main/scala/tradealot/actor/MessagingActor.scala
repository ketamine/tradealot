package tradealot.actor

import akka.actor.{ActorLogging, Actor}
import tradealot.ActorPaths
import tradealot.messages.{InboundMessageRouting, InboundMessage}

abstract class MessagingActor extends Actor with ActorLogging with ActorPaths {

  private var current: Any = _

  def receive = {
    case incomingMessage =>
      current = incomingMessage
      incomingMessage match {
        case message: InboundMessage => handleInboundMessage(message)
        case _ => handleGenericMessage()
      }
  }

  def onInboundMessage: PartialFunction[Any, Unit] = PartialFunction.empty

  def onGenericMessage: PartialFunction[Any, Unit] = PartialFunction.empty

  def currentMessage = current

  def inboundMessage: InboundMessage = current match {
    case msg: InboundMessage => msg
    case _ => notInboundMessage
  }

  def routing: InboundMessageRouting = current match {
    case msg: InboundMessage => msg.routing
    case _ => notInboundMessage
  }

  def senderName : String = sender().path.name

  private def notInboundMessage: Nothing = {
    throw new IllegalStateException("Message being processed is not an inbound message envelope")
  }

  protected def reply(response: Any, failure: Boolean = false) = {
    current match {
      case msg: InboundMessage => msg.reply(response, failure)
      case _ => log.warning("Reply called on message other than InboundMessage")
    }
  }

  private def handleInboundMessage(message: InboundMessage) {
    val content: Any = message.content

    if (onInboundMessage.isDefinedAt(content)) {
      onInboundMessage.apply(content)
    }
  }

  private def handleGenericMessage() {
    if (onGenericMessage.isDefinedAt(current)) {
      onGenericMessage.apply(current)
    }
  }
}
