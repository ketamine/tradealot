package tradealot.actor

import akka.actor.{Actor, ActorLogging, ActorRef, Terminated}
import tradealot.ActorPaths
import tradealot.messages._

import scala.collection.mutable.ArrayBuffer

class User(userId: String) extends Actor with ActorPaths with ActorLogging {

  private var attachedGateway: Option[ActorRef] = None

  private val joinedAuctions = new ArrayBuffer[Int]()
  private val startedAuctions = new ArrayBuffer[Int]()

  override def preStart() {
    notificationDispatcher() ! NotificationSubscriptionRequest()
  }

  def receive = {

    case Terminated(actor) => attachedGateway match {
      case Some(gw) if gw == actor =>
        attachedGateway = None
        log.info("Disconnecting user " + userId)
    }

    case msg: InboundMessage => processMessage(msg)
    case ChannelLinkageRequest(_, gateway) => attachGateway(gateway)

    case msg @ OutgoingMessage(_, AuctionCreated(auctionId), false) =>
      startedAuctions += auctionId
      writeMessage(msg)

    case msg @ OutgoingMessage(_, AuctionJoined(auctionId), false) =>
      joinedAuctions += auctionId
      writeMessage(msg)

    case msg: ChannelWritable => writeMessage(msg)
  }

  private def writeMessage(message: ChannelWritable) = {
    attachedGateway match {
      case Some(gateway) => gateway ! message
      case _ => print("Received message without an attached gateway : " + message)
    }
  }

  private def attachGateway(newGateway: ActorRef) = attachedGateway match {
    case Some(_) =>
      newGateway ! CloseGateway("Only one connection per user is allowed")
    case _ =>
      attachedGateway = Some(newGateway)
      context.watch(newGateway)
      newGateway ! ChannelLinked()
  }


  private def routeCommand(action: String, message: InboundMessage) = {
    action match {
      case Command.GetManufacturers =>
        path("productCatalog") ! message

      case Command.GetActiveAuctions =>
        path("auctionManager") ! message

      case _ => writeMessage(OutgoingMessage(message.routing.messageId, "Unknown Command", failure = true))
    }
  }

  def processMessage(message: InboundMessage) = {
    message.content match {
      case command: Command => routeCommand(command.action, message)
      case _: GetProducts => path("productCatalog") ! message
      case _: AuctionRequest => path("auctionManager") ! message
      case bidRequest: BidRequest => auction(bidRequest.auctionId) ! message
      case joinRequest: JoinAuction => auction(joinRequest.auctionId) ! message
      case gas: GetAuctionStatus => auction(gas.auctionId) ! message
      case _: ParticipationStatusQuery =>
        val status = ParticipationStatus(joinedAuctions.toList, startedAuctions.toList)
        writeMessage(OutgoingMessage(message.routing.messageId, status))
    }
  }

}
