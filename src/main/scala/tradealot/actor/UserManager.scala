package tradealot.actor

import akka.actor.{Actor, ActorRef, Props, Terminated}
import tradealot.messages.ChannelLinkageRequest

import scala.collection.mutable

class UserManager extends Actor {

  private val children = new mutable.HashMap[String, ActorRef]()

  def receive = {
    case linkageRequest: ChannelLinkageRequest => connect(linkageRequest)
    case Terminated(child) => children.retain((k, v) => v != child)
  }

  def connect(linkageRequest: ChannelLinkageRequest) {
    val userId = linkageRequest.userId
    children.get(userId) match {
      case Some(ref) => ref ! linkageRequest
      case None =>
        val props = Props(classOf[User], userId)
        val child = context.actorOf(props, userId)
        child ! linkageRequest
        children += userId -> child
    }
  }
}
