package tradealot

import akka.actor.{ActorSystem, Props}
import io.netty.bootstrap.ServerBootstrap
import io.netty.channel.ChannelInitializer
import io.netty.channel.nio.NioEventLoopGroup
import io.netty.channel.socket.SocketChannel
import io.netty.channel.socket.nio.NioServerSocketChannel
import tradealot.actor.{NotificationDispatcher, AuctionManager, ProductCatalog, UserManager}
import tradealot.network.ServerChannelInitializer
import tradealot.spring.AppContext

object AuctionBootstrap {

  val applicationContext = AppContext.get()
  val system = applicationContext.getBean(classOf[ActorSystem])

  system.actorOf(Props(classOf[UserManager]), "users")
  system.actorOf(Props(classOf[NotificationDispatcher]), "notificationDispatcher")
  system.actorOf(AppContext.props(classOf[ProductCatalog]), "productCatalog")
  system.actorOf(AppContext.props(classOf[AuctionManager]), "auctionManager")

  def main(args: Array[String]): Unit = {

    val bossGroup = new NioEventLoopGroup
    val workerGroup = new NioEventLoopGroup
    try {
      val bootstrap = new ServerBootstrap()

      val handler = new ChannelInitializer[SocketChannel] {
        def initChannel(channel: SocketChannel) {
          val channelInitializer = applicationContext.getBean(classOf[ServerChannelInitializer])
          channel.pipeline().addLast(channelInitializer)
        }
      }

      bootstrap.group(bossGroup, workerGroup)
        .channel(classOf[NioServerSocketChannel])
        .childHandler(handler)

      bootstrap.bind(9696).sync().channel().closeFuture().sync()
    } finally {
      workerGroup.shutdownGracefully()
      bossGroup.shutdownGracefully()
    }

  }


}
