package tradealot.messages

import java.util.Date

import akka.actor.{ActorRef, ActorRefFactory}
import com.fasterxml.jackson.annotation.JsonProperty
import tradealot.ActorPaths

trait ChannelWritable {
  @JsonProperty("messageType")
  def messageType(): String
}
trait Notification extends ChannelWritable {
  override def messageType():String = this.getClass.getSimpleName
}

case class InboundMessageRouting(userId: String, messageId: Int) extends ActorPaths {
  def reply(body: Any, failure: Boolean = false)(implicit context: ActorRefFactory) = {
    val response = OutgoingMessage(messageId, body, failure)
    user(userId) ! response
  }
}

case class InboundMessage(routing: InboundMessageRouting, content: Any) {
  def reply(body: Any, failure: Boolean = false)(implicit context: ActorRefFactory) = {
    routing.reply(body, failure)
  }
}

case class OutgoingMessage(messageId: Int, content: Any, failure: Boolean = false) extends ChannelWritable {
  override def messageType(): String = content.getClass.getSimpleName
}

case class Command(action: String)
object Command {
  val Ping = "Ping"
  val GetManufacturers = "GetManufacturers"
  val GetActiveAuctions = "GetActiveAuctions"
}

case class Bid(auctionId: Int, bidder: String, amount: BigDecimal) extends Notification
case class InternalErrorAlert(message: String = "An internal error occurred")

case class BidRequest(auctionId: Int, amount: BigDecimal)
case class BidAccepted()
case class BidRejected(reason: String)

case class PriceUpdate(auctionId: Int, price: BigDecimal)
case class RemainingTime(auctionId: Int, lastBid: Option[Date], timeout: Option[Int]) extends Notification
case class UpdatePackage(priceUpdates: Map[Int, BigDecimal], finished: List[Int], remainingTimes: List[RemainingTime]) extends Notification

case class AuctionRequest(startingPrice: BigDecimal, productId: Int, condition: Int)
case class AuctionCreated(auctionId: Int)

case class TerminateAuction(auctionId: Int)

case class GetProducts(manufacturer: String)
case class GetAuctionStatus(auctionId: Int)

case class ChannelLinkageRequest(userId: String, gateway: ActorRef)
case class CloseGateway(reason: String)
case class ChannelLinked()

case class NotificationSubscriptionRequest()
case class NotificationSubscribed()
case class JoinAuction(auctionId: Int)
case class AuctionJoined(auctionId: Int)

case class ParticipationStatusQuery()
case class ParticipationStatus(joined: List[Int], started: List[Int])

case class ProductInfo(productId: Int, model: String, manufacturer: String)
case class Lot(auctionId: Int, productInfo: ProductInfo, condition: Int, ownerId: String) extends Notification
case class AuctionInfo(lot: Lot, price: BigDecimal, remainingTime: Option[RemainingTime]) extends Notification
case class AuctionStatus(auctionInfo: AuctionInfo, bids: List[Bid])
case class AuctionFinished(auctionId: Int, winner: String) extends Notification
