package tradealot

import akka.actor.{ActorSelection, ActorRefFactory}

trait ActorPaths {

  val NOTIFICATION_DISPATCHER = "notificationDispatcher"

  def path(address: String) (implicit context: ActorRefFactory): ActorSelection = context.actorSelection("/user/" + address)
  def user(id: String) (implicit context: ActorRefFactory) : ActorSelection = path("users/" + id)
  def auction(id: Int) (implicit context: ActorRefFactory) : ActorSelection = path("auctionManager/" + id)
  def notificationDispatcher() (implicit context: ActorRefFactory) : ActorSelection = path(NOTIFICATION_DISPATCHER)
}
