package tradealot.spring

import akka.actor.{Actor, IndirectActorProducer}
import org.springframework.context.ApplicationContext

class SpringActorProducer[T <: Actor](actorType: Class[T], applicationContext: ApplicationContext, initializer: (T) => Unit)
  extends IndirectActorProducer {

  override def produce(): T = {
    val bean: T = applicationContext.getBean(actorType)
    initializer(bean)
    bean
  }

  override def actorClass: Class[_ <: Actor] = actorType
}
