package tradealot.spring

import akka.actor.Props
import org.springframework.context.ApplicationContext
import org.springframework.context.annotation.AnnotationConfigApplicationContext

object AppContext {
  private val applicationContext = new AnnotationConfigApplicationContext(classOf[ContextConfig])

  def get(): ApplicationContext = applicationContext

  def props[T](actorType: Class[T], initializer: (T) => Unit = {(t: T) => }): Props = {
    Props(classOf[SpringActorProducer[T]], actorType, applicationContext, initializer)
  }
}
