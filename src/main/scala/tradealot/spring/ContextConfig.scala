package tradealot.spring

import javax.sql.DataSource

import akka.actor.ActorSystem
import com.jolbox.bonecp.BoneCPDataSource
import org.springframework.context.annotation.{Scope, Bean, ComponentScan, Configuration}
import org.springframework.jdbc.core.JdbcTemplate
import org.springframework.jdbc.datasource.DataSourceTransactionManager
import org.springframework.transaction.PlatformTransactionManager
import org.springframework.transaction.annotation.EnableTransactionManagement
import tradealot.actor.{AuctionManager, Auction, ProductCatalog}
import tradealot.messages._
import tradealot.network.{ConnectionInitiator, JsonDeserializer, ServerChannelInitializer, StaticFileHandler}
import tradealot.service.{AuctionService, ProductService}

@Configuration
@ComponentScan(Array("tradealot"))
@EnableTransactionManagement
class ContextConfig {

  private val msgClasses = Seq(
    classOf[Command],     classOf[GetProducts],               classOf[AuctionRequest],
    classOf[JoinAuction], classOf[ParticipationStatusQuery],  classOf[GetAuctionStatus],
    classOf[BidRequest]
  )

  @Bean
  def staticFileHandler(): StaticFileHandler = new StaticFileHandler("src/main/static/")

  @Bean
  def channelInitializer(): ServerChannelInitializer = {
    new ServerChannelInitializer(staticFileHandler(), jsonDeserializer(), connectionInitiator())
  }

  @Bean
  def messagingClasses(): Map[String, Class[_ <: AnyRef]] = {
    msgClasses.map(cls => cls.getSimpleName -> cls).toMap
  }

  @Bean
  def jsonDeserializer(): JsonDeserializer = new JsonDeserializer(messagingClasses(), actorSystem())

  @Bean
  def connectionInitiator(): ConnectionInitiator = new ConnectionInitiator(actorSystem())

  @Bean
  def dataSource(): DataSource = {
    val ds = new BoneCPDataSource()
    ds.setDriverClass("com.mysql.jdbc.Driver")
    ds.setUser("root")
    ds.setPassword("1")
    ds.setJdbcUrl("jdbc:mysql://localhost:3306/tradealot")
    ds
  }

  @Bean
  def jdbcTemplate() = new JdbcTemplate(dataSource())

  @Bean
  def transactionManager(): PlatformTransactionManager = new DataSourceTransactionManager(dataSource())

  @Bean
  def productService() = new ProductService(jdbcTemplate())

  @Bean
  def auctionService() = new AuctionService(jdbcTemplate())

  @ActorBean
  def productCatalog() = new ProductCatalog(productService())

  @ActorBean
  def auction() = new Auction(auctionService())

  @ActorBean
  def auctionManager() = new AuctionManager(auctionService(), productService())

  @Bean
  def actorSystem(): ActorSystem = ActorSystem()

}
