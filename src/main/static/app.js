var app = angular.module("tradeALot", ["ui.router"]);

var conditions = {
    1: "Mint",
    2: "A few scratches",
    3: "Heavily used",
    4: "Damaged"
};


app.service("Alerts", function () {

    var timeout = 5000;

    var queue = [];
    var bar = $("#notification-bar");
    var isDisplayed = false;
    var timerId;

    bar.click(function () {
        if (timerId) {
            clearTimeout(timerId);
            hide();
        }
    });

    function popQueue() {
        if (queue.length > 0) {
            var msg = queue.pop();
            animate(msg.message, msg.error);
        }
    }

    function hide() {
        bar.animate({bottom: bar.height() * -1}, function () {
            isDisplayed = false;
            popQueue();
        });
    }

    function animate(message, error) {
        isDisplayed = true;
        bar.removeClass().addClass("alert").addClass(error ? "alert-danger" : "alert-success");
        bar.text(message);
        bar.animate({bottom: 0});
        timerId = setTimeout(function () {
            hide()
        }, timeout);
    }

    return {
        info: function (message) {
            this.notify(message, false);
        },
        error: function (message) {
            this.notify(message, true);
        },
        notify: function (message, error) {
            if (isDisplayed) {
                queue.push({message: message, error: error});
            } else {
                animate(message, error);
            }
        }
    }

});

app.config(function ($stateProvider, $urlRouterProvider) {

    $urlRouterProvider.otherwise('/activeAuctions');

    $stateProvider

        .state('startAuction', {
            url: '/startAuction',
            templateUrl: 'partials/startAuction.html',
            controller: StartAuctionController
        })

        .state('activeAuctions', {
            url: '/activeAuctions',
            templateUrl: 'partials/activeAuctions.html',
            controller: ActiveAuctionsController
        })

        .state('auction', {
            url: '/auction/:auctionId',
            templateUrl: 'partials/liveAuction.html',
            controller: LiveAuctionController
        })

});

app.service("AuctionService", function (RequestService) {

    var startedByMe = [];
    var joinedByMe = [];

    RequestService.request("ParticipationStatusQuery").then(function (data) {
        joinedByMe = joinedByMe.concat(data["joined"]);
        startedByMe = startedByMe.concat(data["started"]);
    });

    return {
        issueCommand: function (action) {
            return RequestService.request("Command", {"action": action});
        },

        placeBid: function (auctionId, amount) {
            return RequestService.request("BidRequest", {"auctionId": auctionId, "amount": amount});
        },
        getManufacturers: function () {
            return this.issueCommand("GetManufacturers");
        },
        getProducts: function (manufacturer) {
            return RequestService.request("GetProducts", {"manufacturer": manufacturer});
        },
        getActiveAuctions: function () {
            return this.issueCommand("GetActiveAuctions");
        },
        getAuctionStatus: function (auctionId) {
            return RequestService.request("GetAuctionStatus", {auctionId: auctionId});
        },
        joinAuction: function (auctionId) {
            var promise = RequestService.request("JoinAuction", {"auctionId": auctionId});

            promise.then(function () {
                joinedByMe.push(auctionId);
            });

            return promise;
        },
        startAuction: function (product, startingPrice, condition) {
            var promise = RequestService.request("AuctionRequest", {
                startingPrice: startingPrice,
                productId: product,
                condition: condition
            });

            promise.then(function (response) {
                startedByMe.push(response.auctionId);
            });

            return promise;
        },
        isStartedByMe: function (auctionId) {
            return _.contains(startedByMe, auctionId);
        },
        isParticipating: function (auctionId) {
            return _.contains(joinedByMe, auctionId);
        }
    }
});


app.service("RequestService", function ($rootScope, $q) {

    var requestCounter = 0;
    var handlers = {};
    var registeredListeners = {};
    var backlog = [];
    var connected = false;

    var messageReceived = function (event) {
        var payload = event.data;
        console.debug("RCV: " + payload);
        var data = JSON.parse(payload);
        var messageId = data['messageId'];
        var failure = data['failure'];
        var content = data['content'];

        if (messageId) {
            var promise = handlers[messageId];

            if (!failure) {
                promise.resolve(content);
            } else {
                promise.reject(content);
            }
        } else {
            var messageType = data['messageType'];
            var group = registeredListeners[messageType];
            if (group) {
                _.each(group, function (listener) {
                    listener(data);
                });
            }
        }

        $rootScope.$apply();
    };

    function openConnection() {
        var connection = new WebSocket("ws://localhost:9696/commence");
        connection.onmessage = messageReceived;
        connection.onopen = function () {
            connected = true;
            backlog.forEach(function (item) {
                WS.send(JSON.stringify(item));
            });
        };

        connection.onclose = function () {
            connected = false;
        };

        return connection;
    }

    var WS = openConnection();

    return {
        request: function (messageType, message) {
            var deferred = $q.defer();
            var requestId = ++requestCounter;
            handlers[requestId] = deferred;

            message = message || {};

            message['messageType'] = messageType;
            message['messageId'] = requestId;

            if (connected) {
                var serializedMessage = JSON.stringify(message);
                console.debug("SND: " + serializedMessage);
                WS.send(serializedMessage);
            } else {
                backlog.push(message);
            }

            return deferred.promise;
        },
        listen: function (messageType, listener) {
            var listenerGroup = registeredListeners[messageType];
            if (!listenerGroup) {
                listenerGroup = [];
                registeredListeners[messageType] = listenerGroup;
            }

            listenerGroup.push(listener);
        },
        stopListening: function (listener) {
            _.each(registeredListeners, function (group, messageType) {
                if (_.contains(group, listener)) {
                    registeredListeners[messageType] = _.without(group, listener);
                }
            })
        }
    }
});

var StartAuctionController = function ($scope, $state, AuctionService, Alerts) {

    AuctionService.getManufacturers().then(function (data) {
        $scope.manufacturerList = data;
    });

    $scope.conditions = conditions;

    $scope.startAuction = function () {
        var startingPrice = parseFloat($scope.startingPrice);

        if (isNaN(startingPrice)) {
            Alerts.error("Invalid price");
            return;
        }

        startingPrice = startingPrice.toFixed(2);

        AuctionService.startAuction($scope.product, startingPrice, $scope.condition)
            .then(function () {
                $state.go("activeAuctions");
                Alerts.info("Auction created!")
            }).catch(function (data) {
                Alerts.error(data.message)
            });
    };


    $scope.$watch("manufacturer", function (value) {
        $scope.product = undefined;
        $scope.productList = undefined;

        if (value) {
            AuctionService.getProducts(value).then(function (data) {
                $scope.productList = data;
            })
        }
    });

    $scope.$watch("product", function (value) {
        if (!value) {
            $scope.condition = undefined;
        }
    });
};

var NavController = function ($scope, $location) {
    $scope.isActive = function (viewLocation) {
        return (viewLocation === $location.path());
    };
};

app.controller("NavController", NavController);

app.directive("remainingTime", function ($interval) {


    var undefinedRemainingTime = "-- : --";

    function pad (input) {
        input = input.toString();
        var padding = "00";
        return padding.substring(0, padding.length - input.length) + input;
    }

    function formatRemainingTime (time) {
        if (time && time > 0) {
            var minutes = pad(Math.floor(time / 60));
            var seconds = pad(time % 60);
            return minutes + " : " + seconds;
        } else {
            return undefinedRemainingTime;
        }
    }

    function calculateRemainingTime (remainingTime) {

        if (!remainingTime) {
            return -1;
        }


        var lastBid = remainingTime.lastBid;
        var timeout = remainingTime.timeout;
        if (!lastBid || !timeout) {
            return -1;
        }

        var secondsElapsed = Math.floor((new Date().getTime() - lastBid) / 1000);
        return timeout - secondsElapsed;
    }


    return {
        restrict: "A",
        template: "<span>{{formattedTime}}</span>",
        scope: {
            remainingTime: "=remainingTime"
        },
        link: function (scope, element, attributes) {

            var remainingTime;

            scope.formattedTime = undefinedRemainingTime;

            scope.$watch("remainingTime", function (val) {
                 remainingTime = calculateRemainingTime(val);
            });

            var intervalPromise = $interval(function () {
                remainingTime--;
                scope.formattedTime = formatRemainingTime(remainingTime);
            }, 1000);

            scope.$on("destroy", function () {
                $interval.cancel(intervalPromise);
            });

        }
    }
});

var ActiveAuctionsController = function ($scope, $state, AuctionService, RequestService, Alerts) {

    $scope.conditions = conditions;
    $scope.auctions = [];


    AuctionService.getActiveAuctions().then(function (data) {
        $scope.auctions = data
    });

    var auctionListener = function (auction) {
        $scope.auctions.push(auction);
    };

    var updateListener = function (update) {
        _.each($scope.auctions, function (auction) {
            var aid = auction.lot.auctionId;
            var newPrice = update.priceUpdates[aid];
            var remainingTime = _.detect(update.remainingTimes, {auctionId: aid});
            if (newPrice) {
                auction.price = newPrice;
            }
            if (remainingTime) {
                auction.remainingTime = remainingTime;
            }
        });

        $scope.auctions = _.reject($scope.auctions, function (auction) {
            return _.contains(update.finished, auction.lot.auctionId);
        });

    };

    RequestService.listen("AuctionInfo", auctionListener);
    RequestService.listen("UpdatePackage", updateListener);

    $scope.$on("destroy", function () {
        RequestService.stopListening(auctionListener);
        RequestService.stopListening(updateListener);
    });

    function goToAuction(auctionId) {
        $state.go("auction", {"auctionId": auctionId});
    }

    $scope.joinAuction = function (auctionId) {

        if (AuctionService.isStartedByMe(auctionId) || AuctionService.isParticipating(auctionId)) {
            goToAuction(auctionId);
            return;
        }

        AuctionService.joinAuction(auctionId)
            .then(function () {
                goToAuction(auctionId);
            })
            .catch(function (message) {
                Alerts.error(message);
            })
    }

};

var LiveAuctionController = function ($scope, $stateParams, $interval, AuctionService, RequestService, Alerts) {
    var auctionId;

    $scope.remainingTime = -1;
    $scope.bids = [];
    $scope.loaded = false;
    $scope.bidInProgress = false;
    $scope.conditions = conditions;

    AuctionService.getAuctionStatus($stateParams.auctionId).then(function (status) {
        var lot = status.auctionInfo.lot;

        var bids = status.bids;

        $scope.remainingTime = status.auctionInfo.remainingTime;

        auctionId = lot.auctionId;

        $scope.loaded = true;
        $scope.lot = lot;
        $scope.bids = bids;
        $scope.currentPrice = status.auctionInfo.price;
    });

    $scope.placeBid = function () {

        var amount;
        try {
            amount = parseFloat($scope.bidAmount).toFixed(2);
        } catch (e) {
            Alerts.error("Invalid amount");
        } finally {
            $scope.bidAmount = undefined;
        }

        $scope.bidInProgress = true;

        AuctionService.placeBid(auctionId, amount).then(function () {
            Alerts.info("Bid placed");
        }).catch(function (response) {
            Alerts.error(response.reason);
        }).finally(function () {
            $scope.bidInProgress = false;
        });
    };

    function bidListener(bid) {
        if (bid.auctionId != auctionId) {
            return;
        }

        $scope.currentPrice = bid.amount;
        $scope.bids.unshift(bid);
    }

    function remainingTimeListener(update) {
        if (update.auctionId != auctionId) {
            return;
        }

        $scope.remainingTime = update
    }

    function finishListener(finished) {
        $scope.winner = finished.winner;
        $scope.finished = true
    }

    RequestService.listen("Bid", bidListener);
    RequestService.listen("RemainingTime", remainingTimeListener);
    RequestService.listen("AuctionFinished", finishListener);

    $scope.$on("destroy", function () {
        RequestService.stopListening(bidListener);
        RequestService.stopListening(remainingTimeListener);
        RequestService.stopListening(finishListener);

    });

};